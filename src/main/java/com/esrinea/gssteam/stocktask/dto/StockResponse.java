/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esrinea.gssteam.stocktask.dto;

/**
 *
 * @author passant.swelum
 */
public class StockResponse <T extends Object> extends Response{
    
    private T data;

    public StockResponse() {
    }

    public StockResponse(int status, String message, T data) {
        setStatus(status);
        setMessage(message);
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
    
}