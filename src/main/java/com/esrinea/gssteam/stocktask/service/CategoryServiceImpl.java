/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esrinea.gssteam.stocktask.service;

import com.esrinea.gssteam.stocktask.dao.CategoryDAO;
import com.esrinea.gssteam.stocktask.dao.ItemDAO;
import com.esrinea.gssteam.stocktask.dao.OperationDAO;
import com.esrinea.gssteam.stocktask.entity.Category;
import com.esrinea.gssteam.stocktask.exception.BusinessException;
import com.esrinea.gssteam.stocktask.util.ErrorConstantsHelper;
import com.esrinea.gssteam.stocktask.util.ValidationHelper;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author passant.swelum
 */
@Service
public class CategoryServiceImpl implements CategoryService{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private CategoryDAO categoryDAO;
    
    @Autowired
    private ItemDAO itemDAO;
    
    @Autowired
    private OperationDAO operationDAO;
    
    @Override
    @Transactional
    public List<Category> getCategories() {
        // return all categories
        return categoryDAO.getCategories(sessionFactory.getCurrentSession());
    }
    
    @Override
    @Transactional
    public Category getCategoryByName(String categoryName) {
        try{
            // throw invalid data exception
            // check validations
            if(!ValidationHelper.isValidName(categoryName)) {
                throw new BusinessException(ErrorConstantsHelper.INVALID_CATEGORY_DATA);
            }
            // if exist return category 
            Category category = categoryDAO.getCategory(sessionFactory.getCurrentSession(), categoryName);
            return category;
        } catch(RuntimeException exception){
            // throw data not found exception 
            throw new BusinessException(ErrorConstantsHelper.CATEGORY_NAME_NOT_FOUND);
        }
       
    }
    
    @Override
    @Transactional
    public Category getCategoryById(int categoryId) {
        try{
            // check validations
            if(!ValidationHelper.isValidNumber(String.valueOf(categoryId))) {
                throw new BusinessException(ErrorConstantsHelper.INVALID_CATEGORY_DATA);
            }
            // if exist return category 
            Category category = categoryDAO.getCategory(sessionFactory.getCurrentSession(), categoryId);
            return category;
        } catch(RuntimeException exception){
            // throw data not found exception 
            throw new BusinessException(ErrorConstantsHelper.CATEGORY_ID_NOT_FOUND);
        }
    }
    
    @Override
    @Transactional
    public int saveCategory(Category category) {
        int id = category.getId();
        try{
            // check validations
            if(ValidationHelper.isValidName(category.getCategoryName()) 
                && ValidationHelper.isValidDescription(category.getCategoryDescription())){
                // get category if exist before saving category
                Category tempCategory = categoryDAO.getCategory(sessionFactory.getCurrentSession(), category.getCategoryName());
                if(tempCategory!=null){
                    // throw data found exception
                    throw new BusinessException(ErrorConstantsHelper.CATEGORY_FOUND);
                }
            } else{
                // throw invalid data exception
                throw new BusinessException(ErrorConstantsHelper.INVALID_CATEGORY_DATA);
            }
        }catch(RuntimeException exception){
            // that means no such a category like this one in db so we save category
            id = categoryDAO.saveCategory(sessionFactory.getCurrentSession(), category);  
        }
        return id;    
    }

    @Override
    @Transactional
    public int updateCategory(Category category) {
        try{
            if(ValidationHelper.isValidName(category.getCategoryName()) 
                && ValidationHelper.isValidDescription(category.getCategoryDescription())){
                int updatedCategoryId = categoryDAO.updateCategory(sessionFactory.getCurrentSession(), category);
                return updatedCategoryId;
            } else{
            // throw invalid data exception
            throw new BusinessException(ErrorConstantsHelper.INVALID_CATEGORY_DATA);
            }     
        } catch(RuntimeException exception){
            // throw data found exception
            throw new BusinessException(ErrorConstantsHelper.CATEGORY_FOUND);
        }
    }

    @Override
    @Transactional
    public void deleteCategory(int categoryId) {
        Session currentSession = sessionFactory.getCurrentSession();
        // delete all operations of this category
        operationDAO.deleteOperationsOfCategory(currentSession, categoryId);
        // delete all items of this category
        itemDAO.deleteItemsOfCategory(currentSession, categoryId);
        // delete this category
        categoryDAO.deleteCategory(currentSession, categoryId);  
    }
    
}
