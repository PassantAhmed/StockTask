/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esrinea.gssteam.stocktask.service;

import com.esrinea.gssteam.stocktask.entity.Item;
import java.util.List;

/**
 *
 * @author passant.swelum
 */
public interface ItemService {
    List<Item> getItems();
    Item getItemByName(String itemName);
    Item getItemById(int itemId);
    int saveItem(Item item);
    int updateItem(Item item);
    void deleteItem(int itemId);
    void deleteItems(int categoryId);
}
