/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esrinea.gssteam.stocktask.service;

import com.esrinea.gssteam.stocktask.entity.Operation;
import com.esrinea.gssteam.stocktask.dto.FiltrationCriterias;
import java.util.List;

/**
 *
 * @author passant.swelum
 */
public interface OperationService {
    List<Operation> getAllOperations();
    List<Operation> getOperations(FiltrationCriterias criterias);
    int saveOperation(Operation operation);
}
