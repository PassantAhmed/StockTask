/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esrinea.gssteam.stocktask.service;

import com.esrinea.gssteam.stocktask.entity.Category;
import java.util.List;

/**
 *
 * @author passant.swelum
 */
public interface CategoryService {
    List<Category> getCategories();
    Category getCategoryByName(String name);
    Category getCategoryById(int categoryId);
    int saveCategory(Category category);
    int updateCategory(Category category);
    void deleteCategory(int categoryId);
}
