/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esrinea.gssteam.stocktask.service;

import com.esrinea.gssteam.stocktask.dao.ItemDAO;
import com.esrinea.gssteam.stocktask.dao.OperationDAO;
import com.esrinea.gssteam.stocktask.entity.Item;
import com.esrinea.gssteam.stocktask.exception.BusinessException;
import com.esrinea.gssteam.stocktask.util.ErrorConstantsHelper;
import com.esrinea.gssteam.stocktask.util.ValidationHelper;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author passant.swelum
 */
@Service
public class ItemServiceImpl implements ItemService{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private ItemDAO itemDAO;
    
    @Autowired
    private OperationDAO operationDAO;
    
    @Override
    @Transactional
    public List<Item> getItems() {
        // return all items
        return itemDAO.getItems(sessionFactory.getCurrentSession());
    }

    @Override
    @Transactional
    public Item getItemByName(String itemName) {
        if(!ValidationHelper.isValidName(itemName)){
            throw new BusinessException(ErrorConstantsHelper.INVALID_ITEM_DATA);
        }
        try{
            // try to get this item 
            Item item = itemDAO.getItem(sessionFactory.getCurrentSession(), itemName);
            return item;
            // throw invalid data exception   
        }catch(RuntimeException exception){
            // throw data not found exception
            throw new BusinessException(ErrorConstantsHelper.ITEM_NAME_NOT_FOUND);
        }  
    }
    
    @Override
    @Transactional
    public Item getItemById(int itemId) {
        if(!ValidationHelper.isValidNumber(String.valueOf(itemId))) {
            // throw invalid data exception
            throw new BusinessException(ErrorConstantsHelper.INVALID_ITEM_DATA);
        }
        try{
            Item item = itemDAO.getItem(sessionFactory.getCurrentSession(), itemId);
            return item;
        } catch(RuntimeException exception){
            // throw data not found exception
            throw new BusinessException(ErrorConstantsHelper.ITEM_ID_NOT_FOUND);
        }
    }

    @Override
    @Transactional
    public int saveItem(Item item) {
        int id = item.getId();
        // check validations
        if(!(ValidationHelper.isValidName(item.getItemName()) 
                && ValidationHelper.isValidDescription(item.getItemDescription()))){
            throw new BusinessException(ErrorConstantsHelper.INVALID_ITEM_DATA);
        }
        // check if the item exists or not
        try{ 
            // check if exist before saving item
            Item checkedItem = itemDAO.getItem(sessionFactory.getCurrentSession(), item.getItemName());
            if(!(checkedItem==null)){
                throw new BusinessException(ErrorConstantsHelper.ITEM_FOUND);
            }
        } catch(RuntimeException exception){
            // that means no such an item like this one in db 
            // save item
            id = itemDAO.saveItem(sessionFactory.getCurrentSession(), item);
        }
        return id;
    }

    @Override
    @Transactional
    public int updateItem(Item item) {
        // check validations
        if(!(ValidationHelper.isValidName(item.getItemName()) 
                && ValidationHelper.isValidDescription(item.getItemDescription()))){
            throw new BusinessException(ErrorConstantsHelper.INVALID_ITEM_DATA);
        }
        try{
            int updatedItemId = itemDAO.updateItem(sessionFactory.getCurrentSession(), item);
            return updatedItemId;
        } catch(RuntimeException exception){
            // throw data found exception
            throw new BusinessException(ErrorConstantsHelper.ITEM_FOUND);
        }
    }

    @Override
    @Transactional
    public void deleteItem(int itemId) {
        Session currentSession = sessionFactory.getCurrentSession();
        // delete operations of this item
        operationDAO.deleteOperationsOfItem(currentSession, itemId);
        // delete item
        itemDAO.deleteItem(currentSession, itemId);
    }

    @Override
    @Transactional
    public void deleteItems(int categoryId) {
        Session currentSession = sessionFactory.getCurrentSession();
        //delete all items operations of this deleted category
        operationDAO.deleteOperationsOfCategory(currentSession, categoryId);
        //delete items of this deleted category
        itemDAO.deleteItemsOfCategory(currentSession, categoryId);
    }
    
}
