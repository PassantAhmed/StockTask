/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esrinea.gssteam.stocktask.util;

/**
 *
 * @author passant.swelum
 */
public class ConstantsHelper {
    
    private ConstantsHelper(){}
    
    public final static char DELETED_COLUMN_VALUE = 'Y';
    public final static char NORMAL_COLUMN_VALUE = 'N';
    
    public final static String OPERATION_TYPE_IN = "in";
    public final static String OPERATION_TYPE_OUT = "out";
    
    public final static String SUCCESS_MESSAGE = "Success";
    public final static int SUCCESS_CODE_RESPONSE = 200;
    public final static int SERVER_ERROR_CODE_RESPONSE = 500;
    public final static int CLIENT_ERROR_CODE_RESPONSE = 400;
    
    public final static String CATEGORY_FILTRATION_QUERY = "and ( item.id IN ( select id from Item where category.id =:categoyId ) ) ";
    public final static String ITEM_FILTRATION_QUERY = "and ( item.id=:itemId ) ";
    public final static String START_DATE_FILTRATION_QUERY = "and ( createdDate<:startDate or createdDate=:startDate ) ";
    public final static String END_DATE_FILTRATION_QUERY = "and ( lastModifiedDate>:endDate or lastModifiedDate=:endDate ) ";
    
}
