/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esrinea.gssteam.stocktask.dao;

import com.esrinea.gssteam.stocktask.entity.Operation;
import com.esrinea.gssteam.stocktask.util.ConstantsHelper;
import com.esrinea.gssteam.stocktask.dto.FiltrationCriterias;
import java.sql.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author passant.swelum
 */
@Repository
public class OperationDAOImpl implements OperationDAO{
    
    private String filtrationQuery;
   
    @Override
    public List<Operation> getOperations(Session session) {
        // select all operations except the deleted ones 
        String stringQuery = "from Operation where deletedFlag=:deletedFlag";
        Query<Operation> operationQuery = session.createQuery(stringQuery,Operation.class);
        operationQuery.setParameter("deletedFlag", ConstantsHelper.NORMAL_COLUMN_VALUE);
        List<Operation> operations = operationQuery.getResultList();
        return operations;
    }

    @Override
    public List<Operation> getFilteredOperations(Session session, FiltrationCriterias criterias) {
        // to get the filtered operations
        filtrationQuery = "from Operation where deletedFlag=:deletedFlag ";
        // we concat queries according to user's choices
        checkFiltrationCriterias(criterias);
        // then concat order by desc 
        filtrationQuery = filtrationQuery+" order by lastModifiedDate desc";
        Query<Operation> operationQuery = session.createQuery(filtrationQuery,Operation.class);
        // set query params according to the concatinations 
        setQueryParams(criterias, operationQuery);
        List<Operation> operations = operationQuery.getResultList();
        return operations;
    }
    
    private void checkFiltrationCriterias(FiltrationCriterias criterias){
        if(criterias.getReportByCategoryId()!=0){
            filtrationQuery = filtrationQuery + ConstantsHelper.CATEGORY_FILTRATION_QUERY;
        }
        if(criterias.getReportByItemId()!=0){
            filtrationQuery = filtrationQuery + ConstantsHelper.ITEM_FILTRATION_QUERY;
        }
        if(criterias.getReportByStartDate()!= null){
            filtrationQuery = filtrationQuery + ConstantsHelper.START_DATE_FILTRATION_QUERY;
        }
        if(criterias.getReportByEndDate()!= null){
            filtrationQuery = filtrationQuery + ConstantsHelper.END_DATE_FILTRATION_QUERY;
        }
    }
    
    private void setQueryParams(FiltrationCriterias criterias, Query<Operation> operationQuery){
        // Adding deleted flag value
        operationQuery.setParameter("deletedFlag", ConstantsHelper.NORMAL_COLUMN_VALUE);
        if(criterias.getReportByCategoryId()!=0){
            operationQuery.setParameter("categoyId", criterias.getReportByCategoryId());
        }
        if(criterias.getReportByItemId()!=0){
            operationQuery.setParameter("itemId", criterias.getReportByItemId());
        }
        if(criterias.getReportByStartDate()!= null){
            operationQuery.setParameter("startDate", criterias.getReportByStartDate());
        }
        if(criterias.getReportByEndDate()!= null){
            operationQuery.setParameter("endDate", criterias.getReportByEndDate());
        }
    }
    
    @Override
    public int saveOperation(Session session, Operation operation) {
        // Adding created and last modified dates & the deleted flag value
        operation.setId(0);
        operation.setCreatedDate(new Date(System.currentTimeMillis()));
        operation.setLastModifiedDate(new Date(System.currentTimeMillis()));
        operation.setDeletedFlag(ConstantsHelper.NORMAL_COLUMN_VALUE);
        session.save(operation);
        return operation.getId();
    }

    @Override
    public void deleteOperationsOfCategory(Session session, int categoryId) {
        // update operations of specific category
        String stringQuery ="update Operation set lastModifiedDate=:lastModifiedDate, deletedFlag=:deletedFlag where "
            + "itemId IN ( select item.id from Item where item.categoryId=:categoryId ";
        Query operationQuery = session.createQuery(stringQuery);
        operationQuery.setParameter("lastModifiedDate", new Date(System.currentTimeMillis()));
        operationQuery.setParameter("deletedFlag", ConstantsHelper.DELETED_COLUMN_VALUE);
        operationQuery.setParameter("categoryId", categoryId);
        // updating data
        operationQuery.executeUpdate();
    }
    
    @Override
    public void deleteOperationsOfItem(Session session, int itemId) {
        String stringQuery ="update Operation set lastModifiedDate=:lastModifiedDate, deletedFlag=:deletedFlag where item.id=:itemId";
        Query operationQuery = session.createQuery(stringQuery);
        operationQuery.setParameter("lastModifiedDate", new Date(System.currentTimeMillis()));
        operationQuery.setParameter("deletedFlag", ConstantsHelper.DELETED_COLUMN_VALUE);
        operationQuery.setParameter("itemId", itemId);
        //updating data
        operationQuery.executeUpdate();
    }
    
}
